export const navlinks = [
  {
    name: 'Agenda',
    route: '/agenda',
    img: 'calender-icon.svg',
  },
  {
    name: 'Message',
    route: '/message',
    img: 'msg-icon.svg',
  },
  {
    name: 'Mentrocasts',
    route: '/mentrocasts',
    img: 'users-icon.svg',
  },
  {
    name: 'Settings',
    route: '/settings',
    img: 'setting-icon.svg',
  },
  {
    name: 'Profile',
    route: '/profile',
    img: 'user-icon.svg',
  },
  {
    name: 'Log Out',
    route: '/logOut',
    img: 'logout-icon.svg',
  },
]

export const explore = [
  {
    name: 'Home',
    route: '/',
  },
  {
    name: 'About',
    route: '/about',
  },
  {
    name: 'Career',
    route: '/career',
  },
]
export const partner = [
  {
    name: 'What do you need',
    route: '/need',
  },
  {
    name: 'How do we work',
    route: '/work',
  },
  {
    name: 'Contibute',
    route: '/contibute',
  },
]
