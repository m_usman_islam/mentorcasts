# Discovered Components

This is an auto-generated list of components discovered by [nuxt/components](https://github.com/nuxt/components).

You can directly use them in pages and other components without the need to import them.

**Tip:** If a component is conditionally rendered with `v-if` and is big, it is better to use `Lazy` or `lazy-` prefix to lazy load.

- `<Profile>` | `<profile>` (components/profile/index.vue)
- `<ProfileInfoGroup>` | `<profile-info-group>` (components/profile/infoGroup.vue)
- `<ProfileSidenav>` | `<profile-sidenav>` (components/profile/sidenav.vue)
- `<JobsCard>` | `<jobs-card>` (components/jobs/card.vue)
- `<JobsFilters>` | `<jobs-filters>` (components/jobs/filters.vue)
- `<JobsHeader>` | `<jobs-header>` (components/jobs/header.vue)
- `<Jobs>` | `<jobs>` (components/jobs/index.vue)
- `<JobsJob>` | `<jobs-job>` (components/jobs/job.vue)
- `<JobsPagination>` | `<jobs-pagination>` (components/jobs/pagination.vue)
- `<JobsRecommand>` | `<jobs-recommand>` (components/jobs/recommand.vue)
