export { default as Profile } from '../..\\components\\profile\\index.vue'
export { default as ProfileInfoGroup } from '../..\\components\\profile\\infoGroup.vue'
export { default as ProfileSidenav } from '../..\\components\\profile\\sidenav.vue'
export { default as JobsCard } from '../..\\components\\jobs\\card.vue'
export { default as JobsFilters } from '../..\\components\\jobs\\filters.vue'
export { default as JobsHeader } from '../..\\components\\jobs\\header.vue'
export { default as Jobs } from '../..\\components\\jobs\\index.vue'
export { default as JobsJob } from '../..\\components\\jobs\\job.vue'
export { default as JobsPagination } from '../..\\components\\jobs\\pagination.vue'
export { default as JobsRecommand } from '../..\\components\\jobs\\recommand.vue'

export const LazyProfile = import('../..\\components\\profile\\index.vue' /* webpackChunkName: "components/profile" */).then(c => c.default || c)
export const LazyProfileInfoGroup = import('../..\\components\\profile\\infoGroup.vue' /* webpackChunkName: "components/profile-info-group" */).then(c => c.default || c)
export const LazyProfileSidenav = import('../..\\components\\profile\\sidenav.vue' /* webpackChunkName: "components/profile-sidenav" */).then(c => c.default || c)
export const LazyJobsCard = import('../..\\components\\jobs\\card.vue' /* webpackChunkName: "components/jobs-card" */).then(c => c.default || c)
export const LazyJobsFilters = import('../..\\components\\jobs\\filters.vue' /* webpackChunkName: "components/jobs-filters" */).then(c => c.default || c)
export const LazyJobsHeader = import('../..\\components\\jobs\\header.vue' /* webpackChunkName: "components/jobs-header" */).then(c => c.default || c)
export const LazyJobs = import('../..\\components\\jobs\\index.vue' /* webpackChunkName: "components/jobs" */).then(c => c.default || c)
export const LazyJobsJob = import('../..\\components\\jobs\\job.vue' /* webpackChunkName: "components/jobs-job" */).then(c => c.default || c)
export const LazyJobsPagination = import('../..\\components\\jobs\\pagination.vue' /* webpackChunkName: "components/jobs-pagination" */).then(c => c.default || c)
export const LazyJobsRecommand = import('../..\\components\\jobs\\recommand.vue' /* webpackChunkName: "components/jobs-recommand" */).then(c => c.default || c)
