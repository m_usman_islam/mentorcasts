import Vue from 'vue'

const components = {
  Profile: () => import('../..\\components\\profile\\index.vue' /* webpackChunkName: "components/profile" */).then(c => c.default || c),
  ProfileInfoGroup: () => import('../..\\components\\profile\\infoGroup.vue' /* webpackChunkName: "components/profile-info-group" */).then(c => c.default || c),
  ProfileSidenav: () => import('../..\\components\\profile\\sidenav.vue' /* webpackChunkName: "components/profile-sidenav" */).then(c => c.default || c),
  JobsCard: () => import('../..\\components\\jobs\\card.vue' /* webpackChunkName: "components/jobs-card" */).then(c => c.default || c),
  JobsFilters: () => import('../..\\components\\jobs\\filters.vue' /* webpackChunkName: "components/jobs-filters" */).then(c => c.default || c),
  JobsHeader: () => import('../..\\components\\jobs\\header.vue' /* webpackChunkName: "components/jobs-header" */).then(c => c.default || c),
  Jobs: () => import('../..\\components\\jobs\\index.vue' /* webpackChunkName: "components/jobs" */).then(c => c.default || c),
  JobsJob: () => import('../..\\components\\jobs\\job.vue' /* webpackChunkName: "components/jobs-job" */).then(c => c.default || c),
  JobsPagination: () => import('../..\\components\\jobs\\pagination.vue' /* webpackChunkName: "components/jobs-pagination" */).then(c => c.default || c),
  JobsRecommand: () => import('../..\\components\\jobs\\recommand.vue' /* webpackChunkName: "components/jobs-recommand" */).then(c => c.default || c)
}

for (const name in components) {
  Vue.component(name, components[name])
  Vue.component('Lazy' + name, components[name])
}
